import React, { Component } from 'react';
import Item from './Item';

class List_Shoe extends Component {
    render() {
        return (
            <div className='container p-5'>
                <h2 className='text-center'>List Shoe</h2>
                <div className="row">
                    {this.props.shoes.map(shoe => {
                        return (
                            <Item addToCart = {()=> {
                                this.props.addToCart(shoe) 
                            }} item = {shoe} />
                        )
                    })}
                </div>
            </div>
        );
    }
}

export default List_Shoe;